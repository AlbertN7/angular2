import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpotifyAuthService } from './service/auth/spotify-auth.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [],
  imports: [CommonModule, HttpClientModule],
})
export class CoreModule {
  constructor(private spotifyAuth: SpotifyAuthService) {
    spotifyAuth.init();
  }
}
