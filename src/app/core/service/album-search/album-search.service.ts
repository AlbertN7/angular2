import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Album } from '../../models/album';
import { SpotifyAuthService } from '../auth/spotify-auth.service';
// import { Observable } from 'rxjs';

interface AlbumResponse {
  albums: {
    items: Album[];
  };
}

@Injectable({
  providedIn: 'root',
})
export class AlbumSearchService {
  constructor(
    private http: HttpClient,
    private spotifyAuth: SpotifyAuthService
  ) {}

  getAlbums(query: string) {
    return this.http.get<AlbumResponse>(`https://api.spotify.com/v1/search`, {
      params: { q: query, type: 'album' },
      headers: {
        Authorization: 'Bearer ' + this.spotifyAuth.getToken(),
      },
    });
  }

  reLogin() {
    this.spotifyAuth.login();
  }
}
