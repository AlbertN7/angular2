import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SpotifyAuthService {
  token: string = '';

  constructor() {}

  init() {
    const token = new URLSearchParams(window.location.hash).get(
      '#access_token'
    );
    if (token) {
      this.token = token;
      sessionStorage.setItem('token', JSON.stringify(token));
    } else {
      this.token = JSON.parse(sessionStorage.getItem('token') || '""');
    }

    if (!this.token) {
      this.login();
    }
  }

  login() {
    const params = new URLSearchParams({
      client_id: 'fc57c707faee4ec28eb39029d908fd79',
      response_type: 'token',
      redirect_uri: 'http://localhost:4200/',
    });
    const url = `https://accounts.spotify.com/authorize?${params}`;

    window.location.href = url;
  }

  getToken() {
    return this.token;
  }
}
