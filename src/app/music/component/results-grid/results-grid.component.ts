import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-results-grid',
  templateUrl: './results-grid.component.html',
  styleUrls: ['./results-grid.component.sass'],
})
export class ResultsGridComponent implements OnInit {
  @Input() albumList!: Array<Object>;

  constructor() {}

  ngOnInit(): void {}
}
