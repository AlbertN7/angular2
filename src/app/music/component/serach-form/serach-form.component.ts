import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-serach-form',
  templateUrl: './serach-form.component.html',
  styleUrls: ['./serach-form.component.sass'],
})
export class SearchFormComponent implements OnInit {
  @Output() serachEvent = new EventEmitter<string>();

  searchValue: string = '';

  serach(searchRef: string) {
    if (searchRef.length > 2) this.serachEvent.emit(searchRef);
  }

  submit() {
    this.serachEvent.emit(this.searchValue);
  }

  constructor() {}

  ngOnInit(): void {}
}
