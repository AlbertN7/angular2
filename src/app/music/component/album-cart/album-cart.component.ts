import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-album-cart',
  templateUrl: './album-cart.component.html',
  styleUrls: ['./album-cart.component.sass'],
})
export class AlbumCartComponent implements OnInit {
  @Input() albumItem!: any;

  constructor() {}

  ngOnInit(): void {}
}
