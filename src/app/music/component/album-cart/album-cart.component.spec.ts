import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumCartComponent } from './album-cart.component';

describe('AlbumCartComponent', () => {
  let component: AlbumCartComponent;
  let fixture: ComponentFixture<AlbumCartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlbumCartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
