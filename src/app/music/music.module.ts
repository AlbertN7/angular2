import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicRoutingModule } from './music-routing.module';
import { MusicComponent } from './music.component';
import { AlbumSearchComponent } from './container/album-search/album-search.component';
import { AlbumCartComponent } from './component/album-cart/album-cart.component';
import { SearchFormComponent } from './component/serach-form/serach-form.component';
import { ResultsGridComponent } from './component/results-grid/results-grid.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    MusicComponent,
    AlbumSearchComponent,
    SearchFormComponent,
    ResultsGridComponent,
    AlbumCartComponent,
  ],
  imports: [CommonModule, MusicRoutingModule, FormsModule],
})
export class MusicModule {}
