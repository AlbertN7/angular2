import { Component, Inject, OnInit } from '@angular/core';
import { AlbumSearchService } from 'src/app/core/service/album-search/album-search.service';

@Component({
  selector: 'app-album-search',
  templateUrl: './album-search.component.html',
  styleUrls: ['./album-search.component.sass'],
  providers: [],
})
export class AlbumSearchComponent implements OnInit {
  albumList: Array<Object> = [];

  constructor(
    @Inject(AlbumSearchService)
    private service: AlbumSearchService
  ) {}

  search(query: string) {
    this.service.getAlbums(query).subscribe({
      next: (res) => {
        this.albumList = res.albums.items;
        console.log(this.albumList[0]);
      },
      error: (err) => {
        let error = err.error.error;
        error.status === 401 ? this.service.reLogin() : console.log(error);
      },
    });
  }

  ngOnInit(): void {}
}
