import { Component, OnInit } from '@angular/core';
import { Playlist } from '../../../core/models/playlist';

@Component({
  selector: 'app-playlist-view',
  templateUrl: './playlist-view.component.html',
  styleUrls: ['./playlist-view.component.sass'],
})
export class PlaylistViewComponent implements OnInit {
  playlist: Playlist[] = [
    {
      id: '1',
      name: 'Playlist 1',
      public: true,
      description: 'Best song ever !',
    },
    {
      id: '2',
      name: 'Playlist 2',
      public: true,
      description: 'Best song ever !',
    },
    {
      id: '3',
      name: 'Playlist 3',
      public: true,
      description: 'Best song ever !',
    },
  ];

  selectedPlayList: Playlist = this.playlist[0];

  mode: 'edit' | 'detail' | 'create' = 'detail';

  edit = () => {
    this.mode = 'edit';
  };

  cancel = () => {
    this.mode = 'detail';
  };

  savePlaylistDraft = (playlistDraft: Playlist) => {
    let index = this.playlist.findIndex((item) => item.id === playlistDraft.id);
    this.playlist[index] = playlistDraft;
    this.selectedPlayList = playlistDraft;
  };

  constructor() {}

  ngOnInit(): void {}
}
