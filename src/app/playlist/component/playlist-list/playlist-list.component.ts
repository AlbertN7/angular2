import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../../core/models/playlist';

@Component({
  selector: 'app-playlist-list',
  templateUrl: './playlist-list.component.html',
  styleUrls: ['./playlist-list.component.sass'],
})
export class PlaylistListComponent implements OnInit {
  @Input('playlistData') playlist: Playlist[] = [];

  @Input() selectedItem!: Playlist;
  @Output() selectedItemChange = new EventEmitter<Playlist>();

  select = (item: Playlist) => {
    this.selectedItem = item;
    this.selectedItemChange.emit(this.selectedItem);
  };

  constructor() {}

  ngOnInit(): void {}
}
