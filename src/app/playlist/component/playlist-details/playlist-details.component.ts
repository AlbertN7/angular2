import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../../core/models/playlist';

@Component({
  selector: 'app-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.sass'],
})
export class PlaylistDetailsComponent implements OnInit {
  @Input('selectedItem') playlistItem!: Playlist;
  @Output() edit = new EventEmitter();

  editEvent = () => {
    this.edit.emit();
  };

  constructor() {}

  ngOnInit(): void {}
}
