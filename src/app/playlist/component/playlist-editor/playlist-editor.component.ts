import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../../core/models/playlist';

@Component({
  selector: 'app-playlist-editor',
  templateUrl: './playlist-editor.component.html',
  styleUrls: ['./playlist-editor.component.sass'],
})
export class PlaylistEditorComponent implements OnInit {
  @Input('selectedItem') playlistItem!: Playlist;

  @Output() cancel = new EventEmitter();
  @Output() savePlaylistDraft = new EventEmitter<Playlist>();

  playlistDraft!: Playlist;
  ngOnChanges(): void {
    this.playlistDraft = { ...this.playlistItem };
  }

  cancelEvent = () => {
    this.cancel.emit();
  };

  saveDraft = () => {
    this.savePlaylistDraft.emit(this.playlistDraft);
  };

  constructor() {}

  ngOnInit(): void {}
}
