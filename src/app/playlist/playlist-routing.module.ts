import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlaylistViewComponent } from './container/playlist-view/playlist-view.component';

const routes: Routes = [
  {
    path: 'playlist',
    component: PlaylistViewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class PlaylistRoutingModule {}
