import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlaylistViewComponent } from './container/playlist-view/playlist-view.component';
import { PlaylistListComponent } from './component/playlist-list/playlist-list.component';
import { PlaylistDetailsComponent } from './component/playlist-details/playlist-details.component';
import { PlaylistEditorComponent } from './component/playlist-editor/playlist-editor.component';
import { PlaylistRoutingModule } from './playlist-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    PlaylistViewComponent,
    PlaylistListComponent,
    PlaylistDetailsComponent,
    PlaylistEditorComponent,
  ],
  imports: [CommonModule, PlaylistRoutingModule, FormsModule],
  exports: [PlaylistViewComponent],
})
export class PlaylistModule {}
